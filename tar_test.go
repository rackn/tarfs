package tarfs

import (
	"io"
	"os"
	"path/filepath"
	"testing"
)

func TestTarOpen(t *testing.T) {
	tars, err := filepath.Glob("testdata/*.tar")
	if err != nil {
		t.Fatalf("Error getting tar list: %v", err)
	}
	expectErrs := map[string]error{
		"testdata/gnu-incremental.tar":     ErrSparseNotImplemented,
		"testdata/gnu-nil-sparse-data.tar": ErrSparseNotImplemented,
		"testdata/gnu-nil-sparse-hole.tar": ErrSparseNotImplemented,
		"testdata/gnu-sparse-big.tar":      ErrSparseNotImplemented,
		"testdata/issue10968.tar":          ErrHeader,
		"testdata/issue11169.tar":          ErrHeader,
		"testdata/issue12435.tar":          ErrHeader,
		"testdata/neg-size.tar":            ErrHeader,
		"testdata/pax-bad-hdr-file.tar":    ErrHeader,
		"testdata/pax-bad-mtime-file.tar":  ErrHeader,
		"testdata/pax-nil-sparse-data.tar": ErrSparseNotImplemented,
		"testdata/pax-nil-sparse-hole.tar": ErrSparseNotImplemented,
		"testdata/pax-nul-path.tar":        ErrHeader,
		"testdata/pax-nul-xattrs.tar":      ErrHeader,
		"testdata/pax-sparse-big.tar":      ErrSparseNotImplemented,
		"testdata/sparse-formats.tar":      ErrSparseNotImplemented,
		"testdata/writer-big-long.tar":     io.ErrUnexpectedEOF,
		"testdata/writer-big.tar":          io.ErrUnexpectedEOF,
	}
	for _, i := range tars {
		fi, err := os.Open(i)
		if err != nil {
			t.Fatalf("Error opening %s: %v", i, err)
		}
		tf, err := Open(fi)
		if err != nil {
			if expect := expectErrs[i]; expect == nil {
				t.Errorf("Error opening tar %s: %v", i, err)
			} else if err != expect {
				t.Errorf("Expected err %v opening %s, not %v", expect, i, err)
			} else {
				t.Logf("Got expected error opening %s", i)
			}
			continue
		}
		if expect := expectErrs[i]; expect != nil {
			t.Errorf("No error opening %s, but expected %v", i, expect)
		} else {
			t.Logf("Opened tar %s", i)
		}
		tf.Close()
	}
}
