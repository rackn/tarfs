package tarfs

import (
	"fmt"
	"io"
	"io/fs"
	"os"
	"path"
	"sort"
	"strings"
	"time"
)

// Src is anything we can use to provide a tarfs
type Src interface {
	fs.File
	io.Seeker
	io.ReaderAt
}

// FS provides access to the contents of an uncompressed tar archive as
// an fs.FS.  Only tar files that do not contain sparse files are supported.
type FS struct {
	r    Src
	curr fileReader
	ents map[string]*inode
	pad  int64
	blk  block
}

var (
	_ = fs.FS(&FS{})
	_ = fs.ReadDirFS(&FS{})
	_ = fs.StatFS(&FS{})
)

func (tf *FS) ensureParentsExist(p string) {
	for i := 256; i > 0; i-- {
		p = path.Dir(p)
		if p == "/" || p == "." || p == "" {
			return
		}
		if _, ok := tf.ents[p]; !ok {
			tf.ents[p] = &inode{
				name:     p,
				fi:       tf.r,
				mode:     int64(0755 | fs.ModeDir),
				typeflag: TypeDir,
				mtime:    time.Now(),
				format:   FormatUnknown,
			}
		}
	}
}

// Open creates a new FS reading from r.
func Open(r Src) (*FS, error) {
	if _, err := r.Seek(0, io.SeekStart); err != nil {
		return nil, err
	}
	defer r.Seek(0, io.SeekStart)
	res := &FS{
		r:    r,
		ents: map[string]*inode{},
		curr: &regFileReader{r, 0},
	}
	res.ents["."] = &inode{
		fi:       r,
		name:     ".",
		mode:     int64(0755 | fs.ModeDir),
		typeflag: TypeDir,
		mtime:    time.Now(),
		format:   FormatUnknown,
	}
	for {
		ino, err := res.next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		switch ino.typeflag {
		case TypeChar, TypeBlock, TypeFifo, TypeXGlobalHeader:
			// hahaha, nope.
			continue
		case TypeReg:
			ino.fi = r
			ino.offset, _ = r.Seek(0, io.SeekCurrent)
		case TypeLink:
			// This is a hard link.  The file to which it refers should already exist.
			ino.linkname = normalizeName(ino.linkname)
			if ri, ok := res.ents[ino.linkname]; !ok || ri.typeflag != TypeReg {
				continue
			} else {
				ino.fi = ri.fi
				ino.size = ri.size
				ino.offset = ri.offset
				ino.mode = ri.mode
				ino.mtime = ri.mtime
				ino.typeflag = TypeReg
				ino.linkname = ""
			}
		case TypeSymlink:
			ino.mode = ino.mode | int64(fs.ModeSymlink)
		case 'D', TypeDir:
			ino.typeflag = TypeDir
			ino.mode = ino.mode | int64(fs.ModeDir)
		default:
			return nil, fmt.Errorf("Cannot handle inode type %c in %s", ino.typeflag, ino.name)
		}
		ino.name = normalizeName(ino.name)
		res.ensureParentsExist(ino.name)
		res.ents[ino.name] = ino
	}
	for _, v := range res.ents {
		if parent, ok := res.ents[path.Dir(v.name)]; !ok {
			return nil, fmt.Errorf("Entry %s has no parent directory", v.name)
		} else if v.name != "." {
			parent.subs = append(parent.subs, v)
		}
	}
	for _, v := range res.ents {
		if !v.IsDir() {
			continue
		}
		sort.Slice(v.subs, func(i, j int) bool { return v.subs[i].Name() < v.subs[j].Name() })
	}
	return res, nil
}

func (tf *FS) Close() error {
	return tf.r.Close()
}

func (tf *FS) next() (*inode, error) {
	var paxHdrs map[string]string
	var gnuLongName, gnuLongLink string

	// Externally, Next iterates through the tar archive as if it is a series of
	// files. Internally, the tar hdrFormat often uses fake "files" to add meta
	// data that describes the next file. These meta data "files" should not
	// normally be visible to the outside. As such, this loop iterates through
	// one or more "header files" until it finds a "normal file".
	hdrFormat := FormatUSTAR | FormatPAX | FormatGNU
	for {
		// Discard the remainder of the file and any padding.
		if err := discard(tf.r, tf.curr.physicalRemaining()); err != nil {
			return nil, err
		}
		if _, err := tryReadFull(tf.r, tf.blk[:tf.pad]); err != nil {
			return nil, err
		}
		tf.pad = 0

		hdr, rawHdr, err := tf.readHeader()
		if err != nil {
			return nil, err
		}
		if err := tf.handleRegularFile(hdr); err != nil {
			return nil, err
		}
		hdrFormat.mayOnlyBe(hdr.format)

		// Check for PAX/GNU special headers and files.
		switch hdr.typeflag {
		case TypeXHeader, TypeXGlobalHeader:
			hdrFormat.mayOnlyBe(FormatPAX)
			paxHdrs, err = parsePAX(tf.curr)
			if err != nil {
				return nil, err
			}
			if hdr.typeflag == TypeXGlobalHeader {
				mergePAX(hdr, paxHdrs)
				return &inode{
					name:       hdr.name,
					typeflag:   hdr.typeflag,
					xattrs:     hdr.xattrs,
					paxrecords: hdr.paxrecords,
					format:     hdrFormat,
				}, nil
			}
			continue // This is a meta header affecting the next header
		case TypeGNULongName, TypeGNULongLink:
			hdrFormat.mayOnlyBe(FormatGNU)
			realname, err := io.ReadAll(tf.curr)
			if err != nil {
				return nil, err
			}

			var p parser
			switch hdr.typeflag {
			case TypeGNULongName:
				gnuLongName = p.parseString(realname)
			case TypeGNULongLink:
				gnuLongLink = p.parseString(realname)
			}
			continue // This is a meta header affecting the next header
		default:
			// The old GNU sparse hdrFormat is handled here since it is technically
			// just a regular file with additional attributes.

			if err := mergePAX(hdr, paxHdrs); err != nil {
				return nil, err
			}
			if gnuLongName != "" {
				hdr.name = gnuLongName
			}
			if gnuLongLink != "" {
				hdr.linkname = gnuLongLink
			}
			if hdr.typeflag == TypeRegA {
				if strings.HasSuffix(hdr.name, "/") {
					hdr.typeflag = TypeDir // Legacy archives use trailing slash for directories
				} else {
					hdr.typeflag = TypeReg
				}
			}

			// The extended headers may have updated the size.
			// Thus, setup the regFileReader again after merging PAX headers.
			if err := tf.handleRegularFile(hdr); err != nil {
				return nil, err
			}

			// Sparse formats rely on being able to read from the logical data
			// section; there must be a preceding call to handleRegularFile.
			if err := tf.handleSparseFile(hdr, rawHdr); err != nil {
				return nil, err
			}

			// Set the final guess at the hdrFormat.
			if hdrFormat.has(FormatUSTAR) && hdrFormat.has(FormatPAX) {
				hdrFormat.mayOnlyBe(FormatUSTAR)
			}
			hdr.format = hdrFormat
			return hdr, nil // This is a file, so stop
		}
	}
}

func (tf *FS) getIno(p string) (*inode, error) {
	p = normalizeName(p)
	if res, ok := tf.ents[p]; ok {
		return res, nil
	}
	return nil, &os.PathError{Path: p, Err: os.ErrNotExist, Op: "open"}
}

func (tf *FS) getInoFollowingSymlinks(p string, count int) (*inode, error) {
	if count == 0 {
		return nil, &fs.PathError{Op: "open", Path: p, Err: fmt.Errorf("Too many symlinks following %s", p)}
	}
	ino, err := tf.getIno(p)
	if err != nil {
		return nil, err
	}
	if ino.typeflag == TypeSymlink {
		if ino.linkname[0] == '/' {
			// absolute links are always processed relative to this filesystem
			ino, err = tf.getInoFollowingSymlinks(ino.linkname, count-1)
		} else {
			// Interpret absence of a leading / as a relative link, and process accordingly.
			ino, err = tf.getInoFollowingSymlinks(path.Join(path.Dir(p), ino.linkname), count-1)
		}
	}
	return ino, err
}

func (tf *FS) Stat(p string) (fs.FileInfo, error) {
	return tf.getInoFollowingSymlinks(p, 128)
}

func (tf *FS) Lstat(p string) (fs.FileInfo, error) {
	return tf.getIno(p)
}

func (tf *FS) ReadLink(p string) (string, error) {
	ent, err := tf.getIno(p)
	if err == nil && ent.typeflag == TypeSymlink {
		return ent.linkname, nil
	}
	return "", &fs.PathError{Op: "readlink", Path: p, Err: fs.ErrNotExist}
}

func (tf *FS) Open(p string) (fs.File, error) {
	ent, err := tf.getInoFollowingSymlinks(p, 128)
	if err != nil {
		return nil, err
	}
	return &File{ino: ent}, nil
}

func (tf *FS) ReadDir(p string) ([]fs.DirEntry, error) {
	ent, err := tf.getIno(p)
	if err == nil && ent.typeflag == TypeDir {
		return ent.subs, nil
	}
	return nil, &fs.PathError{Op: "readdir", Path: p, Err: fs.ErrNotExist}
}
