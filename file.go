package tarfs

import (
	"io"
	"io/fs"
	"syscall"
)

type File struct {
	ino    *inode
	offset int64
}

func (f *File) ReadAt(b []byte, offset int64) (int, error) {
	if f.ino.typeflag != TypeReg {
		return 0, syscall.EINVAL
	}
	if offset >= f.ino.size {
		return 0, io.EOF
	}
	maxRead := int64(len(b))
	if offset+maxRead > int64(f.ino.size) {
		maxRead = int64(f.ino.size) - offset
	}

	// just read the requested number of bytes and change our offset
	readSize, err := f.ino.fi.ReadAt(b[:maxRead], f.ino.offset+offset)
	if readSize < len(b) && err == nil {
		err = io.EOF
	}
	return readSize, err
}

func (f *File) Read(b []byte) (int, error) {
	rs, err := f.ReadAt(b, f.offset)
	f.offset += int64(rs)
	return rs, err
}

func (f *File) Close() error {
	return nil
}

func (f *File) Seek(offset int64, whence int) (int64, error) {
	if f.ino.typeflag != TypeReg {
		return 0, syscall.EINVAL
	}
	var no int64
	switch whence {
	case io.SeekStart:
		no = offset
	case io.SeekEnd:
		no = f.ino.size + offset
	case io.SeekCurrent:
		no = f.offset + offset
	}
	if no < 0 || no > f.ino.size {
		return f.offset, syscall.EINVAL
	}
	f.offset = no
	return f.offset, nil
}

func (f *File) Stat() (fs.FileInfo, error) {
	return f.ino, nil
}

func (f *File) ReadDir(count int) ([]fs.DirEntry, error) {
	if !f.ino.IsDir() || int(f.offset) >= len(f.ino.subs) {
		return nil, io.EOF
	}
	if count <= 0 {
		f.offset = 0
		return f.ino.subs, nil
	}
	if int(f.offset)+count > len(f.ino.subs) {
		count = len(f.ino.subs) - int(f.offset)
	}
	res := f.ino.subs[int(f.offset):count]
	f.offset += int64(count)
	return res, nil
}
